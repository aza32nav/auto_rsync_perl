# Auto Rsync con Perl

## Descripción

El programa actualiza de forma automática las carpetas descritas
en los archivos -\*.arsync- contenido es la carpeta __File__ ubicada
en la misma carpeta que el código auto\_rsync\_perl.

Los archivos tiene una estructura delimitada por **:** y comienza
con la carpeta de origen y la carpeta destino para después contener
las carpetas a actualizar, con el nombre en el origen y seguido del
nombre que tendrá en la carpeta destino. Yo suelo usar el mismo para
evitar confusiones.

## Ejemplo de la estructura de un archivo home.arsync

    /home/user/:/media/hardisk\_extern/
    Downloads/:Downloads/
    Documents/:Documents/
    Music/:Music/
    PersonalFolder/:PersonalFolder/

**Importante** no olvidar poner la **/** al final de cada nombre para
evitar problemas con el uso de rsync.

Se puede utilizar cualquier carpeta como origen como /home/user/Music/
e igual cualquiera como destino, por ejemplo usb, disco duro externo,
o hasta una carpeta del mismo computador solo asegurate que las carpetas
que quieras respaldar se encuentre bien su nombre para evitar incidencias
en los datos, aunque por lo general, nada mas no la respaldaría a menos que
pusieras el nombre de otra carpeta y se copiara en su lugar.

El programa solo funciona con carpetas en fisico, no tiene funcionalidad
por medio de ssh o red aún.

# Objetivo

El objetivo principal del programa es permitir tener respaldos en
discos duros externos de las carpetas que desee de forma automática,
utilizando la propiedad de actualización de rsync para tener una imagen
fiable de tú carpeta respaldada en un disco duro externo y ademas poder
actualizar los cambios que hagas día a día corriendo el programa con tu
archivo configurado.

# Pendientes
- [ ] Agregar manejo de errores
